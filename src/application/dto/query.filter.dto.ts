import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class QueryFilterDto {
  @IsString()
  @IsOptional()
  @ApiPropertyOptional({
    example: 'name',
    description: 'Sort by field',
  })
  public sortField?: string;

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'DESC', description: 'Order by field' })
  public sortOrder?: string = 'DESC';

  @IsString()
  @IsOptional()
  @ApiPropertyOptional({ example: 'Alex', description: 'Filter by name' })
  public name?: string;
}
