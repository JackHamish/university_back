import { Body, Controller, Post, HttpCode, HttpStatus, Query } from '@nestjs/common';
import { AuthService } from '../../services/auth/auth.service';
import { SignRequestDto } from './dto/sign.request.dto';
import { SignResponseDto } from './dto/sign.response.dto';
import { ResetPasswordRequestDto } from './dto/reset-password.request.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ResetPasswordResponseDto } from './dto/reset-password.response.dto';
@ApiTags('Auth')
@ApiResponse({
  status: HttpStatus.INTERNAL_SERVER_ERROR,
  description: 'Something went wrong',
})
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Unauthorized exception' })
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  public async signIn(@Body() signInDto: SignRequestDto): Promise<SignResponseDto> {
    return await this.authService.signIn(signInDto.email, signInDto.password);
  }

  @ApiOperation({ summary: 'Sign up' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Sign up successful',
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-up')
  public async signUp(@Body() signUpDto: SignRequestDto): Promise<void> {
    await this.authService.signUp(signUpDto.email, signUpDto.password);
  }

  @ApiOperation({ summary: 'Reset password request' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Successful send reset link to email',
  })
  @HttpCode(HttpStatus.OK)
  @Post('reset-password-request')
  public async resetPasswordRequest(
    @Body() resetPasswordDto: ResetPasswordRequestDto,
  ): Promise<void> {
    await this.authService.resetPasswordRequest(resetPasswordDto.email);
  }

  @ApiOperation({ summary: 'Reset password' })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Reset password successful',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password')
  public async resetPassword(
    @Body() resetPasswordDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    await this.authService.resetPassword(resetPasswordDto);
  }
}
