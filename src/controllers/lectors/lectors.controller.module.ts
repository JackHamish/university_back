import { Module } from '@nestjs/common';
import { LectorsModule } from '../../services/lectors/lectors.module';
import { LectorsController } from './lectors.controller';
import { LectorsControllerService } from './lectors.controller.service';

@Module({
    imports: [LectorsModule],
    controllers: [LectorsController],
    providers: [LectorsControllerService],
})
export class LectorsControllerModule {}
