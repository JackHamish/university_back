import { Injectable } from '@nestjs/common';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { CreateLectorDto } from 'src/services/lectors/dto/create-lector.dto';
import { UpdateLectorDto } from 'src/services/lectors/dto/update-lector.dto';
import { Lector } from 'src/services/lectors/entities/lector.entity';
import { LectorsService } from 'src/services/lectors/lectors.service';
import { DeleteResult, UpdateResult } from 'typeorm';

@Injectable()
export class LectorsControllerService {
  constructor(private readonly lectorsService: LectorsService) {}

  async create(createLectorDto: CreateLectorDto): Promise<Lector> {
    return await this.lectorsService.create(createLectorDto);
  }

  async findOne(id: string): Promise<Lector> {
    return await this.lectorsService.findOne(id);
  }

  async getAll(queryFilter: QueryFilterDto): Promise<Lector[]> {
    return await this.lectorsService.getAll(queryFilter);
  }

  async update(id: string, updateLectorDto: UpdateLectorDto): Promise<UpdateResult> {
    return await this.lectorsService.update(id, updateLectorDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.lectorsService.remove(id);
  }
}
