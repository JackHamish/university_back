import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseFilters,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { LectorsControllerService } from './lectors.controller.service';
import { CreateLectorDto } from '../../services/lectors/dto/create-lector.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  OmitType,
} from '@nestjs/swagger';
import { Lector } from 'src/services/lectors/entities/lector.entity';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { UpdateLectorDto } from 'src/services/lectors/dto/update-lector.dto';
import { GetLectorResponseDto } from 'src/services/lectors/dto/get-lector-response.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { TypeOrmFilter } from 'src/application/exeptions/typeorm-exeption.filter';

@ApiTags('Lectors')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@UseFilters(TypeOrmFilter)
@UseInterceptors(ClassSerializerInterceptor)
@Controller('lectors')
export class LectorsController {
  constructor(private readonly lectorsControllerService: LectorsControllerService) {}

  @ApiOperation({ summary: 'Create lector' })
  @ApiResponse({
    status: 201,
    description: 'The Lector',
    type: Lector,
  })
  @Post()
  async create(@Body() createLectorDto: CreateLectorDto): Promise<Lector> {
    return await this.lectorsControllerService.create(createLectorDto);
  }

  @ApiOperation({ summary: 'Get lector by id' })
  @ApiResponse({
    status: 200,
    description: 'The Lector',
    type: GetLectorResponseDto,
  })
  @ApiResponse({
    status: 404,
    description: 'Lector was not found',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Lector> {
    return await this.lectorsControllerService.findOne(id);
  }

  @ApiOperation({ summary: 'Get lectors list' })
  @ApiResponse({
    status: 200,
    description: 'The list of lectors',
    type: GetLectorResponseDto,
  })
  @Get()
  async getAll(@Query() queryFilter: QueryFilterDto): Promise<Lector[]> {
    return await this.lectorsControllerService.getAll(queryFilter);
  }

  @ApiOperation({ summary: 'Update lector' })
  @ApiResponse({
    status: 404,
    description: 'Lector was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Update Successful',
  })
  @HttpCode(204)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateLectorDto: UpdateLectorDto,
  ): Promise<void> {
    await this.lectorsControllerService.update(id, updateLectorDto);
  }

  @ApiOperation({ summary: 'Delete lector' })
  @ApiResponse({
    status: 404,
    description: 'Lector was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Delete Successful',
  })
  @HttpCode(204)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.lectorsControllerService.remove(id);
  }
}
