import { Injectable } from '@nestjs/common';
import { CoursesService } from '../../services/courses/courses.service';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { Course } from 'src/services/courses/entities/course.entity';
import { UpdateCourseDto } from 'src/services/courses/dto/update-course.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class CoursesControllerService {
  constructor(private readonly coursesService: CoursesService) {}

  async create(createCourseDto: CreateCourseDto): Promise<Course> {
    return await this.coursesService.create(createCourseDto);
  }

  async findAll(queryFilter: QueryFilterDto): Promise<Course[]> {
    return await this.coursesService.findAll(queryFilter);
  }

  async findOne(id: string): Promise<Course> {
    return await this.coursesService.findOne(id);
  }

  async update(id: string, updateCourseDto: UpdateCourseDto): Promise<UpdateResult> {
    return await this.coursesService.update(id, updateCourseDto);
  }

  async delete(id: string): Promise<DeleteResult> {
    return await this.coursesService.delete(id);
  }
}
