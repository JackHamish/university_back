import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  Patch,
  Query,
  Delete,
  HttpCode,
  UseFilters,
} from '@nestjs/common';
import { CreateCourseDto } from '../../services/courses/dto/create-course.dto';
import { CoursesControllerService } from './courses.controller.service';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { LectorsCoursesService } from '../../services/lector-course/lector-course.service';
import { AddLectorToCourseDto } from '../../services/courses/dto/add-lector-to-course.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Course } from 'src/services/courses/entities/course.entity';
import { UpdateCourseDto } from 'src/services/courses/dto/update-course.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { TypeOrmFilter } from 'src/application/exeptions/typeorm-exeption.filter';

@ApiTags('Courses')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@UseFilters(TypeOrmFilter)
@Controller('courses')
export class CoursesController {
  constructor(
    private readonly coursesControllerService: CoursesControllerService,
    private readonly lectorCoursesService: LectorsCoursesService,
  ) {}

  @ApiOperation({ summary: 'Create course' })
  @ApiResponse({
    status: 201,
    description: 'Created Successfully',
    type: Course,
  })
  @Post()
  async create(@Body() createCourseDto: CreateCourseDto): Promise<Course> {
    return await this.coursesControllerService.create(createCourseDto);
  }

  @ApiOperation({ summary: 'Find course' })
  @ApiResponse({
    status: 200,
    description: 'The list of the courses',
    type: Course,
  })
  @Get()
  async findAll(@Query() queryFilter: QueryFilterDto): Promise<Course[]> {
    return await this.coursesControllerService.findAll(queryFilter);
  }

  @ApiOperation({ summary: 'Find course by id' })
  @ApiResponse({
    status: 200,
    description: 'The list of the courses',
    type: Course,
  })
  @ApiResponse({
    status: 404,
    description: 'Course was not found',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Course> {
    return await this.coursesControllerService.findOne(id);
  }

  @ApiOperation({ summary: 'Update Course data' })
  @ApiResponse({
    status: 200,
    description: 'Successful updated',
  })
  @ApiResponse({
    status: 404,
    description: 'Course not found',
  })
  @HttpCode(204)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateCourseDto: UpdateCourseDto,
  ): Promise<void> {
    await this.coursesControllerService.update(id, updateCourseDto);
  }

  @ApiOperation({ summary: 'Delete Course ' })
  @ApiResponse({
    status: 200,
    description: 'Successful deleted',
  })
  @ApiResponse({
    status: 404,
    description: 'Course not found',
  })
  @HttpCode(204)
  @Delete(':id')
  async delete(@Param('id') id: string): Promise<void> {
    await this.coursesControllerService.delete(id);
  }

  @ApiOperation({ summary: 'Add lector to Course' })
  @ApiResponse({
    status: 200,
    description: 'Successful added lector to course',
  })
  @ApiResponse({
    status: 404,
    description: 'Course or Lector was not found',
  })
  @HttpCode(204)
  @Patch(':id/lector')
  async addLectorToCourse(
    @Param('id') courseId: string,
    @Body() addLectorToCourseDto: AddLectorToCourseDto,
  ): Promise<void> {
    await this.lectorCoursesService.addLectorToCourse(
      courseId,
      addLectorToCourseDto.lectorId,
    );
  }
}
