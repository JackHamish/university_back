import { Module } from '@nestjs/common';
import { CoursesController } from './courses.controller';
import { CoursesControllerService } from './courses.controller.service';
import { CoursesModule } from '../../services/courses/courses.module';
import { LectorsCoursesModule } from 'src/services/lector-course/lector-course.module';

@Module({
  imports: [CoursesModule, LectorsCoursesModule],
  controllers: [CoursesController],
  providers: [CoursesControllerService],
})
export class CoursesControllerModule {}
