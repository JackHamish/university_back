import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { CreateMarkDto } from './../../services/marks/dto/create-mark.dto';
import { Injectable } from '@nestjs/common';
import { UpdateMarkDto } from 'src/services/marks/dto/update-mark.dto';
import { Mark } from 'src/services/marks/entities/mark.entity';
import { MarksService } from 'src/services/marks/marks.service';
import { DeleteResult, UpdateResult } from 'typeorm';

@Injectable()
export class MarksControllerService {
  constructor(private readonly marksService: MarksService) {}

  async create(createMarkDto: CreateMarkDto): Promise<Mark> {
    return await this.marksService.create(createMarkDto);
  }

  async findOne(id: string): Promise<Mark> {
    return await this.marksService.findOne(id);
  }

  async getAll(queryFilter: QueryFilterDto): Promise<Mark[]> {
    return await this.marksService.getAll(queryFilter);
  }

  async update(id: string, updateMarkDto: UpdateMarkDto): Promise<UpdateResult> {
    return await this.marksService.update(id, updateMarkDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.marksService.remove(id);
  }
}
