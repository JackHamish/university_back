import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Patch,
  Post,
  Query,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { MarksControllerService } from './marks.controller.service';
import { CreateMarkDto } from 'src/services/marks/dto/create-mark.dto';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { Mark } from 'src/services/marks/entities/mark.entity';
import { MarksStudentResponseDto } from '../../services/marks/dto/student-response.dto';
import { MarksCourseResponseDto } from '../../services/marks/dto/course-response.dto';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { UpdateMarkDto } from 'src/services/marks/dto/update-mark.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { TypeOrmFilter } from 'src/application/exeptions/typeorm-exeption.filter';

@ApiExtraModels(MarksCourseResponseDto, MarksStudentResponseDto)
@ApiTags('Marks')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@UseFilters(TypeOrmFilter)
@Controller('marks')
export class MarksController {
  constructor(private readonly marksControllerService: MarksControllerService) {}

  @ApiOperation({ summary: 'Create mark' })
  @ApiResponse({
    status: 201,
    description: 'Created Successfully',
    type: Mark,
  })
  @Post()
  async create(@Body() createMarksDto: CreateMarkDto): Promise<Mark> {
    return await this.marksControllerService.create(createMarksDto);
  }

  @ApiOperation({ summary: 'Get Mark by id' })
  @ApiResponse({
    status: 200,
    description: 'The Lector',
    type: Mark,
  })
  @ApiResponse({
    status: 404,
    description: 'Mark was not found',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Mark> {
    return await this.marksControllerService.findOne(id);
  }

  @ApiOperation({ summary: 'Get marks' })
  @ApiResponse({
    status: 200,
    description: 'The list of the marks',
    isArray: true,
    type: Mark,
  })
  @Get()
  async getAll(@Query() queryFilter: QueryFilterDto): Promise<Mark[]> {
    return await this.marksControllerService.getAll(queryFilter);
  }

  @ApiOperation({ summary: 'Update mark' })
  @ApiResponse({
    status: 404,
    description: 'Mark was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Update Successful',
  })
  @HttpCode(204)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateMarkDto: UpdateMarkDto,
  ): Promise<void> {
    await this.marksControllerService.update(id, updateMarkDto);
  }

  @ApiOperation({ summary: 'Delete lector' })
  @ApiResponse({
    status: 404,
    description: 'Lector was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Delete Successful',
  })
  @HttpCode(204)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.marksControllerService.remove(id);
  }
}
