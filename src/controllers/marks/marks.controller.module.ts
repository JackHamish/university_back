import { MarksModule } from 'src/services/marks/marks.module';
import { MarksController } from './marks.controller';
import { MarksControllerService } from './marks.controller.service';
import { Module } from '@nestjs/common';

@Module({
    imports: [MarksModule],
    controllers: [MarksController],
    providers: [MarksControllerService],
})
export class MarksControllerModule {}
