import { Injectable } from '@nestjs/common';
import { GroupsService } from '../../services/groups/groups.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';
import { Group } from 'src/services/groups/entities/group.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class GroupsControllerService {
  constructor(private readonly groupsService: GroupsService) {}

  async create(createGroupDto: CreateGroupDto): Promise<Group> {
    return await this.groupsService.create(createGroupDto);
  }

  async findAll(queryFilter: QueryFilterDto): Promise<Group[]> {
    return await this.groupsService.findAll(queryFilter);
  }

  async findOne(id: string): Promise<Group> {
    return await this.groupsService.findOne(id);
  }

  async update(id: string, updateGroupDto: UpdateGroupDto): Promise<UpdateResult> {
    return await this.groupsService.update(id, updateGroupDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.groupsService.remove(id);
  }
}
