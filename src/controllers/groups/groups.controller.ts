import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  UseGuards,
  Query,
  UseFilters,
} from '@nestjs/common';
import { GroupsControllerService } from './groups.controller.service';
import { CreateGroupDto } from '../../services/groups/dto/create-group.dto';
import { UpdateGroupDto } from '../../services/groups/dto/update-group.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
  OmitType,
} from '@nestjs/swagger';
import { Group } from '../../services/groups/entities/group.entity';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { TypeOrmFilter } from 'src/application/exeptions/typeorm-exeption.filter';

@ApiTags('Groups')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@UseFilters(TypeOrmFilter)
@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsControllerService: GroupsControllerService) {}

  @ApiOperation({ summary: 'Create Group' })
  @ApiResponse({
    status: 200,
    description: 'The Group',
    type: Group,
  })
  @Post()
  async create(@Body() createGroupDto: CreateGroupDto): Promise<Group> {
    return await this.groupsControllerService.create(createGroupDto);
  }

  @ApiOperation({ summary: 'Get all groups' })
  @ApiResponse({
    status: 200,
    description: 'The list of groups',
    type: Group,
  })
  @Get()
  async findAll(@Query() queryFilter: QueryFilterDto): Promise<Group[]> {
    return await this.groupsControllerService.findAll(queryFilter);
  }

  @ApiOperation({ summary: 'Get group by id' })
  @ApiResponse({
    status: 200,
    description: 'The group',
    type: Group,
  })
  @ApiResponse({
    status: 404,
    description: 'Group was not found',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Group> {
    return await this.groupsControllerService.findOne(id);
  }

  @ApiOperation({ summary: 'Update group' })
  @ApiResponse({
    status: 404,
    description: 'Group was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Update Successful',
  })
  @HttpCode(204)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<void> {
    await this.groupsControllerService.update(id, updateGroupDto);
  }

  @ApiOperation({ summary: 'Delete group' })
  @ApiResponse({
    status: 404,
    description: 'Group was not found',
  })
  @ApiResponse({
    status: 204,
    description: 'Delete Successful',
  })
  @HttpCode(204)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.groupsControllerService.remove(id);
  }
}
