import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: '1', description: 'The id of the user' })
  userId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'example@email.com',
    description: 'The email of the user',
  })
  email: string;
}
