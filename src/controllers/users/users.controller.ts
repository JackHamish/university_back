import { Controller, Get, UseGuards } from '@nestjs/common';
import { UsersControllerService } from './users.controller.service';
import { CurrentUser } from '../../services/auth/decorators/current.user.decorator';
import { AuthGuard } from '../../services/auth/guards/auth.guard';
import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Lector } from 'src/services/lectors/entities/lector.entity';
import { UserResponseDto } from './dto/user-response.dto';
@ApiTags('User')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@Controller('')
@ApiBearerAuth('jwt')
export class UsersController {
  constructor(private readonly usersControllerService: UsersControllerService) {}

  @ApiOperation({ summary: 'Get user data' })
  @ApiResponse({
    status: 200,
    description: 'Get Successfully',
    type: UserResponseDto,
  })
  @Get('me')
  public async findMe(@CurrentUser() loggedUser: Lector): Promise<UserResponseDto> {
    return await this.usersControllerService.findCurrentUser(loggedUser.id);
  }
}
