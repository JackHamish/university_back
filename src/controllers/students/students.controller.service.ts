import { Injectable } from '@nestjs/common';
import { StudentsService } from '../../services/students/students.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import { Student } from 'src/services/students/entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class StudentsControllerService {
  constructor(private readonly studentsService: StudentsService) {}
  async create(createStudentDto: CreateStudentDto): Promise<Student> {
    return await this.studentsService.create(createStudentDto);
  }

  async findAll(queryFilter: QueryFilterDto): Promise<Student[]> {
    return await this.studentsService.findAll(queryFilter);
  }

  async findOne(id: string): Promise<Student> {
    return await this.studentsService.findOne(id);
  }

  async update(id: string, updateStudentDto: UpdateStudentDto): Promise<UpdateResult> {
    return await this.studentsService.update(id, updateStudentDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    return await this.studentsService.remove(id);
  }
}
