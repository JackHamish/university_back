import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpCode,
  UseGuards,
  UseFilters,
} from '@nestjs/common';
import { StudentsControllerService } from './students.controller.service';
import { CreateStudentDto } from '../../services/students/dto/create-student.dto';
import { UpdateStudentDto } from '../../services/students/dto/update-student.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Student } from '../../services/students/entities/student.entity';
import { AuthGuard } from 'src/services/auth/guards/auth.guard';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';
import { TypeOrmFilter } from 'src/application/exeptions/typeorm-exeption.filter';

@ApiTags('Students')
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiResponse({
  status: 401,
  description: 'Unauthorized',
})
@ApiResponse({
  status: 400,
  description: 'Validation failed',
})
@ApiResponse({
  status: 500,
  description: 'Internal server error',
})
@UseFilters(TypeOrmFilter)
@Controller('students')
export class StudentsController {
  constructor(private readonly studentsControllerService: StudentsControllerService) {}

  @ApiOperation({ summary: 'Create Student' })
  @ApiResponse({
    status: 201,
    description: 'Created Successfully',
    type: Student,
  })
  @Post()
  async create(@Body() createStudentDto: CreateStudentDto): Promise<Student> {
    return await this.studentsControllerService.create(createStudentDto);
  }

  @ApiOperation({ summary: 'Get all students' })
  @ApiResponse({
    status: 200,
    description: 'The list of the students',
    type: Student,
  })
  @Get()
  async findAll(@Query() queryFilter: QueryFilterDto): Promise<Student[]> {
    return await this.studentsControllerService.findAll(queryFilter);
  }

  @ApiOperation({ summary: 'Get student by id' })
  @ApiResponse({
    status: 200,
    description: 'The student',
    type: Student,
  })
  @ApiResponse({
    status: 404,
    description: 'The student was not found',
  })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Student> {
    return await this.studentsControllerService.findOne(id);
  }

  @ApiOperation({ summary: 'Update student' })
  @ApiResponse({
    status: 204,
    description: 'Updated Successfully',
  })
  @ApiResponse({
    status: 404,
    description: 'The student was not found',
  })
  @HttpCode(204)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateStudentDto: UpdateStudentDto,
  ): Promise<void> {
    await this.studentsControllerService.update(id, updateStudentDto);
  }

  @ApiOperation({ summary: 'Delete student' })
  @ApiResponse({
    status: 204,
    description: 'Deleted Successfully',
  })
  @ApiResponse({
    status: 404,
    description: 'The student was not found',
  })
  @HttpCode(204)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.studentsControllerService.remove(id);
  }
}
