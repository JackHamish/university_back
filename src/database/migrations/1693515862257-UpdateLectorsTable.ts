import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateLectorsTable1693515862257 implements MigrationInterface {
    name = 'UpdateLectorsTable1693515862257'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "name" DROP NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors" ALTER COLUMN "name" SET NOT NULL`);
    }

}
