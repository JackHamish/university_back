import { IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class CreateStudentDto {
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    example: 'alex@email.com',
    description: 'The email of the Student',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Alex',
    description: 'The name of the Student',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Smith',
    description: 'The surname of the Student',
  })
  surname: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    example: 20,
    description: 'The age of the Student',
  })
  age: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({
    description: 'Student image',
    nullable: true,
  })
  imagePath: string;

  @IsOptional()
  @IsNumber()
  @ApiPropertyOptional({
    description: 'Id of the student group',
    nullable: true,
  })
  groupId?: number;
}
