import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Student } from '../entities/student.entity';

export class GetStudentResponseDto extends OmitType(Student, ['groupId'] as const) {
  @ApiProperty({ example: 'LC-91', description: 'The name of student group' })
  groupName: string;
}
