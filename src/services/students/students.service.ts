import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UseFilters,
} from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindManyOptions, Repository, UpdateResult } from 'typeorm';
import { Student } from './entities/student.entity';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  async create(createStudentDto: CreateStudentDto): Promise<Student> {
    const student = await this.studentRepository.findOne({
      where: {
        email: createStudentDto.email,
      },
    });

    if (student) {
      throw new BadRequestException('Student with this email already exist');
    }

    return this.studentRepository.save(createStudentDto);
  }

  async findAll(queryFilter: QueryFilterDto): Promise<Student[]> {
    const findOptions: FindManyOptions<Student> = {};

    if (queryFilter.sortField && queryFilter.sortOrder) {
      findOptions.order = {
        [queryFilter.sortField]: queryFilter.sortOrder,
      };
    }

    if (queryFilter.name) {
      findOptions.where = {
        name: queryFilter.name,
      };
    }

    return await this.studentRepository.find(findOptions);
  }

  async findOne(id: string): Promise<Student> {
    const student = await this.studentRepository.findOne({
      where: {
        id,
      },
    });

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return student;
  }

  async update(id: string, updateStudentDto: UpdateStudentDto): Promise<UpdateResult> {
    await this.findOne(id);

    return await this.studentRepository.update(id, updateStudentDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    await this.findOne(id);

    return await this.studentRepository.delete(id);
  }

  async findAllWithGroupName(name: string): Promise<Student[]> {
    let students = await this.studentRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email ',
        'student.age as age ',
        'student.image_path as "imagePath"',
        'student.created_at as "createdAt"',
        'student.updated_at as "updatedAt"',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .getRawMany();

    if (name) {
      students = students.filter((student) => student.name === name);
    }

    return students;
  }

  async findOneWithGroupName(id: string): Promise<Student> {
    const student = await this.studentRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email ',
        'student.age as age ',
        'student.image_path as "imagePath"',
        'student.created_at as "createdAt"',
        'student.updated_at as "updatedAt"',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    return student;
  }
}
