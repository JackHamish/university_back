import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Course } from '../../courses/entities/course.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @ApiProperty({ example: 'Alex', description: 'The name of the student' })
  @Index()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @ApiProperty({ example: 'Smith', description: 'The surname of the student' })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @ApiProperty({ example: 'example@email.com', description: 'The email of the student' })
  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  email: string;

  @ApiProperty({ example: 20, description: 'The age of the student' })
  @Column({
    type: 'numeric',
    nullable: false,
  })
  age: number;

  @ApiProperty({ example: 'image.png', description: 'The image of the student' })
  @Column({
    type: 'varchar',
    nullable: true,
    name: 'image_path',
  })
  imagePath: string;

  @ApiProperty({ example: 1, description: 'The id of the student group' })
  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @ManyToMany(() => Course, (course) => course.students, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'student_course',
    joinColumn: {
      name: 'student_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];

  @OneToMany(() => Mark, (mark) => mark.student, {
    cascade: ['remove'],
  })
  @JoinColumn()
  marks: Mark[];
}
