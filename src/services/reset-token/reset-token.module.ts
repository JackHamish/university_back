import { Module } from '@nestjs/common';
import { ResetTokenService } from './reset-token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Token } from './entities/token.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Token])],
  providers: [ResetTokenService],
  exports: [ResetTokenService],
})
export class ResetTokenModule {}
