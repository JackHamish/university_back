import { Injectable, Logger } from '@nestjs/common';
import crypto from 'crypto';

import { ResetTokenInterface } from './interfaces/reset-token.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Token } from './entities/token.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    @InjectRepository(Token)
    private readonly tokensRepository: Repository<Token>,
    private jwtService: JwtService,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  public async generateResetToken(email: string): Promise<ResetTokenInterface> {
    // const token = crypto.randomBytes(32).toString('hex');

    const token = await this.jwtService.signAsync({ email });

    const resetPasswordObject = {
      email,
      token,
    };

    return await this.tokensRepository.save(resetPasswordObject);
  }

  public async getResetToken(token: string): Promise<ResetTokenInterface> {
    return await this.tokensRepository.findOne({ where: { token } });
  }

  public async removeResetToken(token: string): Promise<void> {
    const { id } = await this.tokensRepository.findOne({ where: { token } });
    await this.tokensRepository.delete(id);
  }
}
