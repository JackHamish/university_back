import { Column, Entity } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';

@Entity({ name: 'tokens' })
export class Token extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  token: string;
}
