import { CoreEntity } from 'src/application/entities/core.entity';

export interface ResetTokenInterface extends CoreEntity {
  email: string;
  token: string;
}
