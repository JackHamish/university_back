import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { DeleteResult, FindManyOptions, Repository, UpdateResult } from 'typeorm';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  async create(createCourseDto: CreateCourseDto): Promise<Course> {
    const course = await this.courseRepository.findOne({
      where: {
        name: createCourseDto.name,
      },
    });

    if (course) {
      throw new BadRequestException('Course with this name already exist');
    }

    return await this.courseRepository.save(createCourseDto);
  }

  async findAll(queryFilter: QueryFilterDto): Promise<Course[]> {
    const findOptions: FindManyOptions<Course> = {};

    if (queryFilter.sortField && queryFilter.sortOrder) {
      findOptions.order = {
        [queryFilter.sortField]: queryFilter.sortOrder,
      };
    }

    if (queryFilter.name) {
      findOptions.where = {
        name: queryFilter.name,
      };
    }
    return await this.courseRepository.find(findOptions);
  }

  async update(id: string, updateCourseDto: UpdateCourseDto): Promise<UpdateResult> {
    await this.findOne(id);

    return await this.courseRepository.update(id, updateCourseDto);
  }

  async delete(id: string): Promise<DeleteResult> {
    await this.findOne(id);

    return await this.courseRepository.delete(id);
  }

  async findOne(id: string): Promise<Course> {
    const course = await this.courseRepository.findOne({
      where: {
        id,
      },
    });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async findOneWithLectors(id: string) {
    const course = await this.courseRepository.findOne({
      relations: {
        lectors: true,
      },
      where: {
        id,
      },
    });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async findByLectorId(lectorId: string) {
    const courses = await this.courseRepository.find({
      where: {
        lectors: {
          id: lectorId,
        },
      },
    });

    return courses;
  }

  async save(saveCourseDto: CreateCourseDto) {
    return await this.courseRepository.save(saveCourseDto);
  }
}
