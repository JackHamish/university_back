import { Column, Entity, JoinColumn, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @ApiProperty({ example: 'Chemistry', description: 'The name of the Course' })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @ApiProperty({
    example: 'Something about course',
    description: 'The description of the Course',
  })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @ApiProperty({ example: 60, description: 'The hours of the Course' })
  @Column({
    type: 'numeric',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Student, (student) => student.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  students?: Student[];

  @ManyToMany(() => Lector, (lector) => lector.courses, { onDelete: 'CASCADE' })
  lectors?: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course, {
    nullable: false,
    eager: false,
  })
  @JoinColumn()
  marks: Mark[];
}
