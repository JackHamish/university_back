import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AddLectorToCourseDto {
  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  lectorId: string;
}
