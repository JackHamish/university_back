import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCourseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'Chemistry', description: 'The name of the Course' })
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    example: 'Something about course',
    description: 'The description of the Course',
  })
  description: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({ example: 60, description: 'The hours of the Course' })
  hours: number;
}
