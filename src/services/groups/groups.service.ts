import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, FindManyOptions, Repository, UpdateResult } from 'typeorm';
import { Group } from './entities/group.entity';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupRepository: Repository<Group>,
  ) {}

  async create(createGroupDto: CreateGroupDto): Promise<Group> {
    const group = await this.groupRepository.findOne({
      where: {
        name: createGroupDto.name,
      },
    });

    if (group) {
      throw new BadRequestException('Group with this name already exist');
    }
    return this.groupRepository.save(createGroupDto);
  }
  async findAll(queryFilter: QueryFilterDto): Promise<Group[]> {
    const findOptions: FindManyOptions<Group> = {};

    if (queryFilter.sortField && queryFilter.sortOrder) {
      findOptions.order = {
        [queryFilter.sortField]: queryFilter.sortOrder,
      };
    }

    if (queryFilter.name) {
      findOptions.where = {
        name: queryFilter.name,
      };
    }

    return await this.groupRepository.find(findOptions);
  }

  async findOne(id: string): Promise<Group> {
    const group = await this.groupRepository.findOne({
      where: {
        id,
      },
    });

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    return group;
  }

  async update(id: string, updateGroupDto: UpdateGroupDto): Promise<UpdateResult> {
    await this.findOne(id);

    return await this.groupRepository.update(id, updateGroupDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    await this.findOne(id);

    return await this.groupRepository.delete(id);
  }

  async findOneWithStudents(id: string) {
    const group = await this.groupRepository.find({
      where: {
        id,
      },
      relations: {
        students: true,
      },
    });

    if (!group?.length) {
      throw new NotFoundException('Group not found');
    }

    return group;
  }

  async findAllWithStudents() {
    return await this.groupRepository.find({
      relations: {
        students: true,
      },
    });
  }
}
