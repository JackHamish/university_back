import { Column, Entity, OneToMany } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'groups' })
export class Group extends CoreEntity {
  @ApiProperty({ example: 'LC-91', description: 'The name of the group' })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @OneToMany(() => Student, (student) => student.group, {
    cascade: ['remove'],
  })
  students: Student[];
}
