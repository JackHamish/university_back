import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateGroupDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'LC-21', description: 'The name of the Group' })
  name: string;
}
