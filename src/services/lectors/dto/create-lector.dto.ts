import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
  IsStrongPassword,
  IsOptional,
} from 'class-validator';

export class CreateLectorDto {
  @ApiPropertyOptional({ example: 'John', description: 'The name of the lector' })
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty({ example: 'example@email.com', description: 'The email of the lector' })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ example: 'P1a2s3w4o5r6d!', description: 'The password of the lector' })
  @IsNotEmpty()
  @IsStrongPassword()
  password: string;
}
