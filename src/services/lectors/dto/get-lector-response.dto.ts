import { OmitType } from '@nestjs/swagger';
import { Lector } from '../entities/lector.entity';

export class GetLectorResponseDto extends OmitType(Lector, ['password'] as const) {}
