import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { DeleteResult, FindManyOptions, Repository, UpdateResult } from 'typeorm';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorDto } from './dto/update-lector.dto';
import * as bcrypt from 'bcrypt';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async create(createLectorDto: CreateLectorDto): Promise<Lector> {
    const lector = await this.lectorRepository.findOne({
      where: {
        email: createLectorDto.email,
      },
    });

    const salt = await bcrypt.genSalt();

    createLectorDto.password = await bcrypt.hash(createLectorDto.password, salt);

    if (lector) {
      throw new BadRequestException('Email already exist');
    }

    return this.lectorRepository.save(createLectorDto);
  }

  async findOne(id: string): Promise<Lector> {
    const lector = this.lectorRepository.findOne({
      where: {
        id,
      },
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async findByEmail(email: string): Promise<Lector> {
    const lector = this.lectorRepository.findOne({
      where: {
        email,
      },
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async getAll(queryFilter: QueryFilterDto): Promise<Lector[]> {
    const findOptions: FindManyOptions<Lector> = {};

    if (queryFilter.sortField && queryFilter.sortOrder) {
      findOptions.order = {
        [queryFilter.sortField]: queryFilter.sortOrder,
      };
    }

    if (queryFilter.name) {
      findOptions.where = {
        name: queryFilter.name,
      };
    }

    return await this.lectorRepository.find(findOptions);
  }

  async update(id: string, updateLectorDto: UpdateLectorDto): Promise<UpdateResult> {
    await this.findOne(id);

    if (updateLectorDto.password) {
      const salt = await bcrypt.genSalt();

      updateLectorDto.password = await bcrypt.hash(updateLectorDto.password, salt);
    }

    return await this.lectorRepository.update(id, updateLectorDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    await this.findOne(id);

    return await this.lectorRepository.delete(id);
  }

  async findOneWithCourses(id: string) {
    const lector = this.lectorRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }
}
