import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToMany } from 'typeorm';
import { Course } from '../../courses/entities/course.entity';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Mark } from '../../marks/entities/mark.entity';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @ApiPropertyOptional({ example: 'John', description: 'The name of the lector' })
  @Column({
    type: 'varchar',
    nullable: true,
  })
  name: string;

  @ApiProperty({ example: 'example@email.com', description: 'The email of the lector' })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Exclude()
  @ApiProperty({ example: 'P1a2s3w4o5r6d!', description: 'The password of the lector' })
  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'lector_course',
    joinColumn: {
      name: 'lector_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector, {
    nullable: false,
    eager: false,
    cascade: true,
  })
  @JoinColumn()
  marks: Mark[];
}
