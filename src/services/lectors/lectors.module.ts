import { Module } from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Lector])],
    providers: [LectorsService],
    exports: [LectorsService],
})
export class LectorsModule {}
