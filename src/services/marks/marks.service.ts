import { UpdateMarkDto } from './dto/update-mark.dto';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { DeleteResult, FindManyOptions, Repository, UpdateResult } from 'typeorm';
import { CreateMarkDto } from './dto/create-mark.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly marksRepository: Repository<Mark>,
  ) {}

  async create(createMarkDto: CreateMarkDto): Promise<Mark> {
    const { studentId, courseId, lectorId } = createMarkDto;

    const existingMark = await this.marksRepository.findOne({
      where: {
        studentId,
        lectorId,
        courseId,
      },
    });

    if (existingMark) {
      throw new BadRequestException('Mark already exist');
    }

    return this.marksRepository.save(createMarkDto);
  }

  async getAll(queryFilter: QueryFilterDto): Promise<Mark[]> {
    const findOptions: FindManyOptions<Mark> = {};

    if (queryFilter.sortField && queryFilter.sortOrder) {
      findOptions.order = {
        [queryFilter.sortField]: queryFilter.sortOrder,
      };
    }

    return await this.marksRepository.find(findOptions);
  }

  async findOne(id: string): Promise<Mark> {
    const mark = this.marksRepository.findOne({
      where: {
        id,
      },
    });

    if (!mark) {
      throw new NotFoundException('Mark not found');
    }

    return mark;
  }

  async update(id: string, updateMarkDto: UpdateMarkDto): Promise<UpdateResult> {
    await this.findOne(id);

    return await this.marksRepository.update(id, updateMarkDto);
  }

  async remove(id: string): Promise<DeleteResult> {
    await this.findOne(id);

    return await this.marksRepository.delete(id);
  }

  async getStudentMarksById(studentId: string) {
    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .select('mark.mark', 'mark')
      .addSelect('course.name', 'courseName')
      .leftJoin('mark.course', 'course')
      .where('mark.studentId = :studentId', { studentId })
      .getRawMany();

    return marks;
  }

  async getCourseMarksById(courseId: string) {
    const marks = await this.marksRepository
      .createQueryBuilder('mark')
      .leftJoin('mark.course', 'course')
      .leftJoin('mark.lector', 'lector')
      .leftJoin('mark.student', 'student')
      .select('course.name', 'courseName')
      .addSelect('lector.name', 'lectorName')
      .addSelect('student.name', 'studentName')
      .addSelect('mark.mark', 'mark')
      .where('course.id = :courseId', { courseId })
      .getRawMany();

    return marks;
  }
}
