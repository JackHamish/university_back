import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreateMarkDto {
  @ApiProperty({ example: 100, description: 'The mark' })
  @IsNotEmpty()
  @IsNumber()
  mark: number;

  @ApiProperty({ example: '1', description: 'The id of the student' })
  @IsNotEmpty()
  @IsString()
  studentId: string;

  @ApiProperty({ example: '1', description: 'The id of the course' })
  @IsNotEmpty()
  @IsString()
  courseId: string;

  @ApiProperty({ example: '1', description: 'The id of the lector' })
  @IsNotEmpty()
  @IsString()
  lectorId: string;
}
