import { ApiProperty, PickType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Mark } from '../entities/mark.entity';

export class MarksStudentResponseDto extends PickType(Mark, ['mark'] as const) {
  @ApiProperty({ example: 'Logic', description: 'The name of the course' })
  @IsNotEmpty()
  @IsString()
  courseName: string;
}
