import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { MarksStudentResponseDto } from './student-response.dto';

export class MarksCourseResponseDto extends MarksStudentResponseDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'Alex', description: 'The name of the lector' })
  lectorName: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ example: 'John', description: 'The name of the student' })
  studentName: string;
}
