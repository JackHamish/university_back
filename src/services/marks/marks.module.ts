import { Module } from '@nestjs/common';
import { MarksService } from './marks.service';
import { MarksController } from '../../controllers/marks/marks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Mark])],
    providers: [MarksService],
    exports: [MarksService],
})
export class MarksModule {}
