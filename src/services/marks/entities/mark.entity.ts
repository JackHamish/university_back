import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../../application/entities/core.entity';
import { Course } from '../../courses/entities/course.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { ApiProperty } from '@nestjs/swagger';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @ApiProperty({ example: 100, description: 'The mark' })
  @Column({
    type: 'integer',
    nullable: false,
  })
  mark: number;

  @ApiProperty({ example: '1', description: 'The id of the course' })
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'course_id',
  })
  courseId: string;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ApiProperty({ example: '1', description: 'The id of the student' })
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'student_id',
  })
  studentId: string;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ApiProperty({ example: '1', description: 'The id of the lector' })
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: string;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
