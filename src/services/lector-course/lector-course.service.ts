import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { LectorsService } from '../lectors/lectors.service';
import { CoursesService } from '../courses/courses.service';

@Injectable()
export class LectorsCoursesService {
  constructor(
    private readonly lectorsService: LectorsService,
    private readonly coursesService: CoursesService,
  ) {}

  async addLectorToCourse(courseId: string, lectorId: string) {
    const lector = await this.lectorsService.findOne(lectorId);
    const course = await this.coursesService.findOne(courseId);

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    if (!course.lectors) course.lectors = [];

    const isLectorExist = course.lectors.find((lector) => lector.id === lectorId);

    if (isLectorExist) {
      throw new BadRequestException('Lector already exist on course');
    }

    course.lectors.push(lector);

    return await this.coursesService.save(course);
  }
}
