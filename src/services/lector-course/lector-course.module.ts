import { Module } from '@nestjs/common';
import { LectorsCoursesService } from './lector-course.service';
import { LectorsModule } from '../lectors/lectors.module';
import { CoursesModule } from '../courses/courses.module';

@Module({
    imports: [LectorsModule, CoursesModule],
    providers: [LectorsCoursesService],
    exports: [LectorsCoursesService],
})
export class LectorsCoursesModule {}
