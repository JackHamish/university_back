import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { SignResponseDto } from '../../controllers/auth/dto/sign.response.dto';
import { ResetTokenInterface } from '../reset-token/interfaces/reset-token.interface';
import { ResetPasswordWithTokenRequestDto } from '../../controllers/auth/dto/reset-password-with-token.request.dto';
import { LectorsService } from '../lectors/lectors.service';
import * as bcrypt from 'bcrypt';
import { MailService } from '../mail/mail.service';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private mailService: MailService,
  ) {}

  public async signUp(email: string, pass: string): Promise<SignResponseDto> {
    const lector = await this.lectorsService.create({ email, password: pass });

    const payload = { sub: lector.id, email: lector.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(email: string): Promise<ResetTokenInterface> {
    const lector = await this.lectorsService.findByEmail(email);
    if (!lector) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user ${email} is not found`,
      );
    }
    const response = await this.resetTokenService.generateResetToken(email);

    await this.mailService.sendResetPasswordLink(email, response.token);

    return response;
  }

  public async resetPassword(
    resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    const { token, newPassword } = resetPasswordWithTokenRequestDto;
    const { email } = this.jwtService.decode(token) as ResetTokenInterface;

    const resetPasswordRequest = await this.resetTokenService.getResetToken(token);

    if (!resetPasswordRequest) {
      throw new BadRequestException(
        `There is no request password request for user: ${email}`,
      );
    }
    const lector = await this.lectorsService.findByEmail(email);
    if (!lector) {
      throw new BadRequestException(`User is not found`);
    }

    const salt = await bcrypt.genSalt();

    const hashNewPassword = await bcrypt.hash(newPassword, salt);

    await this.lectorsService.update(lector.id, { password: hashNewPassword });

    await this.resetTokenService.removeResetToken(token);
  }

  public async signIn(email: string, pass: string): Promise<SignResponseDto> {
    const lector = await this.lectorsService.findByEmail(email);

    const checkPassword = await bcrypt.compare(pass, lector.password);

    if (!checkPassword) {
      throw new UnauthorizedException();
    }
    const payload = { userId: lector.id, email: lector.email };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }
}
